# RMXP to TileD Autotile Converter
Converts autotiles in RMXP format into an expanded set for use with TileD.

![image](https://gitlab.com/ZenVirZan/rmxptiledautotileconverter/-/raw/master/banner.png)