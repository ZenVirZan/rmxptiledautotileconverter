﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection.PortableExecutable;

public class Program
{
    public static int Main(string[] args)
    {
        if (args.Length < 2)
        {
            Console.WriteLine("Usage: <input directory> <output directory> [-f]");
            Console.WriteLine("Flags:");
            Console.WriteLine("\t-f\tOverwrite destination files");
            return 0;
        }

        var inputDirectory = args[0];
        var outputDirectory = args[1];
        if (!Directory.Exists(inputDirectory))
            Console.WriteLine($"Unable to find input directory \"{inputDirectory}\"");
        if (!Directory.Exists(outputDirectory))
            Console.WriteLine($"Unable to find output directory \"{outputDirectory}\"");

        var force = false;
        if (args.Length == 3)
            force = args[2] == "-f";

        if (force)
            Console.WriteLine("Output file overwrite permitted");

        var translationTable = new int[,]
        {
            { 26,27,4,27,26,5,4,5,26,27,4,27,26,5,4,5 },
            { 32,33,32,33,32,33,32,33,32,11,32,11,32,11,32,11 },
            { 26,27,4,27,26,5,4,5,26,27,4,27,26,5,4,5 },
            { 10,33,10,33,10,33,10,33,10,11,10,11,10,11,10,11 },
            { 24,25,24,5,24,25,24,5,14,15,14,15,14,15,14,15 },
            { 30,31,30,31,30,11,30,11,20,21,20,11,10,21,10,11 },
            { 28,29,28,29,4,29,4,29,38,39,4,39,38,5,4,5 },
            { 34,35,10,35,34,35,10,35,44,45,44,45,44,45,44,45 },
            { 24,29,14,15,12,13,12,13,16,17,16,17,40,41,4,41 },
            { 30,35,44,45,18,19,18,11,22,23,10,23,46,47,46,47 },
            { 36,37,36,5,12,17,12,13,36,41,16,17,12,17,0,1 },
            { 42,43,42,43,18,23,42,43,42,47,46,47,42,47,6,7 }
        };

        foreach (var inputFile in Directory.EnumerateFiles(inputDirectory))
        {
            if (!File.Exists(inputFile)) continue;

            var filename = Path.GetFileName(inputFile);
            var filenameNoExt = Path.GetFileNameWithoutExtension(inputFile);
            var fileExt = Path.GetExtension(inputFile);

            if (new[] { ".png", ".jpg" }.Contains(fileExt.ToLower()))
            {
                Console.WriteLine($"Converting \"{filename}\"...");

                var image = Image.FromFile(inputFile);
                var frames = image.Width / 96;
                var newImage = new Bitmap(256 * frames, 192);
                var g = Graphics.FromImage(newImage);

                for (int f = 0; f < frames; f++)
                    for (int x = 0; x < 256 / 16; x++)
                        for (int y = 0; y < 192 / 16; y++)
                        {
                            var v = translationTable[y, x];
                            g.DrawImage(image,
                                new Rectangle(x * 16 + (256 * f), y * 16, 16, 16),
                                new Rectangle((v % 6) * 16, (v / 6) * 16, 16, 16),
                                GraphicsUnit.Pixel);
                        }

                g.Save();
                g.Dispose();

                var outFilename = string.Join("/", outputDirectory, filenameNoExt + ".png");
                if (File.Exists(outFilename)
                    && !force)
                {
                    Console.WriteLine($"Skipping, destination file already exists: \"{outFilename}\"");
                }
                else
                {
                    newImage.Save(outFilename, ImageFormat.Png);
                }
            }
        }

        return 0;
    }
}
